<?php

add_action ( 'init', 'register_post_types' );
function register_post_types() {
	register_post_type ( 'slides', array (
			'labels' => array (
					'name' => 'Slides' 
			),
			'public' => true 
	) );
}

add_filter('the_content', 'bad_word_filter');

function bad_word_filter($content) {
	$bad_words = array( 'douchebag', 'wally' );
    $content = str_ireplace($bad_words, '*****', $content);
    return $content;
}

add_filter('the_content', 'good_word_filter');

function good_word_filter($content) {
	$bad_words = array( 'bad', 'worse' );
	$good_words = array( 'good', 'better' );
	$content = str_ireplace($bad_words, $good_words, $content);
	return $content;
}

add_action( 'wp_footer', 'prowp_site_analytics' );
function prowp_site_analytics() { ?>
 <script type="text/javascript"> var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); document.write(unescape("%3Cscript src='" + gaJsHost + 'google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); </script> <script type="text/javascript"> var pageTracker = _gat._getTracker("UA-XXXXXX-XX"); pageTracker._trackPageview();</script> 
<?php } 

?>
