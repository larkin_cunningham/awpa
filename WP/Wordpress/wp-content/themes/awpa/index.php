<?php get_header(); ?>
<div id="main">
	<div id="content">
		<h1>Posts</h1>
		     <?php
				if (have_posts ()) {
					while ( have_posts () ) {
						the_post ();
						?>   <!-- The wordpress �loop� --!>
			           <h2><?php the_title(); ?></h2>
	                   <h4>Posted on <?php the_time('F j, Y') ?></h4>
	                   <p><?php the_content(__('(more...)')); ?></p>
	                   <hr> 
	                   <?php get_template_part( 'content', get_post_format() ); ?>
      <?php
  			        }
  			    }
				else {
								?>
           <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
           <?php
                } ?>
     </div>   
     <?php get_sidebar('right');
		get_template_part('twitter','index');
     ?>
</div>
<div id="delimiter"></div>
<?php get_footer(); ?>
