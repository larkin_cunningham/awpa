<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm-fzX*~|7{xb-Dy4e<R%+d>Kl+OF;4|OT]r;7rWE[wUJ+ETp.Ncl{84%gVOlF-Sq');
define('SECURE_AUTH_KEY',  'FlzZ7xIWsz#B-0I7X]De=To.kf%~$qRXdqY]DnWm< A7kfB-IyiXs^<X-90]Q1>d');
define('LOGGED_IN_KEY',    'F-gp 5fvrF7VM*`UZf4*/kgGS VG.PNy+7VEye<Y@wIG=#r8eJ>,&6|Xjo5+lLht');
define('NONCE_KEY',        'Anq}JP}5??>s(w966$z^kWpw9)S}BUj?Zp+[x3#m%8?~aN]YH5j.mN%au3|DPUqx');
define('AUTH_SALT',        '<}R27Y`UEeBORchI;+pQ-b!s^$+ywef./!iD)Jd<x[YzLW|A8!oK99(9VZ`kBTJJ');
define('SECURE_AUTH_SALT', 'rY8k2++nlT-Yh OV/q4q8{J]]~2LYK0xRJ^iabe8{0pvl<h,.S;U`hAIJ r3?:lf');
define('LOGGED_IN_SALT',   '*j>Y=I+F}#:aCz+B/n9fc7]ksgS}5K3K|]lF~c:=X!@inl8)G=M:HOI-*{SpYY!*');
define('NONCE_SALT',       '*E&w<L-fZ[^E2O,jh[|1O)0t:;7fy,,^[K6|^=Jlu07r.|}8qCKmT9jsek1/9sFS');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
